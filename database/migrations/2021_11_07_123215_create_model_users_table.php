<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModelUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuario', function (Blueprint $table) {
            $table->increments('usr_id');
            $table->string('usr_nome');
            $table->string('usr_email')->unique();
            $table->string('usr_cpf')->unique();
            $table->date('usr_data_nascimento');
            $table->string('usr_telefone');
            $table->string('usr_endereco');
            $table->string('usr_cidade');
            $table->string('usr_estado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuario');
    }
}
