<!doctype html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Lista de Usuários</title>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
    <div>
        <h1 class="text-center">Lista de Usuários</h1>
        <hr>
        <div class="container col-8 m-auto">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nome</th>
                    <th scope="col">Cpf</th>
                    <th scope="col">Data de Nascimento</th>
                    <th scope="col">Email</th>
                    <th scope="col">Telefone</th>
                    <th scope="col">Endereço</th>
                    <th scope="col">Cidade</th>
                    <th scope="col">Estado</th>
                </tr>
                </thead>
                <tbody>
                @foreach($usuarios as $usuario)
                    <tr>
                        <th scope="row">{{$usuario->usr_id}}</th>
                        <td>{{$usuario->usr_nome}}</td>
                        <td>{{$usuario->usr_cpf}}</td>
                        <td>{{$usuario->usr_data_nascimento}}</td>
                        <td>{{$usuario->usr_email}}</td>
                        <td>{{$usuario->usr_telefone}}</td>
                        <td>{{$usuario->usr_endereco}}</td>
                        <td>{{$usuario->usr_cidade}}</td>
                        <td>{{$usuario->usr_estado}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

    </div>

</body>
</html>
