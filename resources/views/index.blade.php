<!doctype html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cadastro de Usuario</title>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.0/jquery.mask.js"></script>
    <script src="http://127.0.0.1:8000/assets/getEstadoseCidades.js"></script>
    <script src="http://127.0.0.1:8000/assets/mask.js"></script>
</head>
<body>
<div>
    <h1 class="text-center">Cadastro de Usuário</h1>
    <hr>
    <div class="container col-8 m-auto">
        <form action="http://127.0.0.1:8000/store" method="POST">
            @csrf

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputNome">Nome</label>
                    <input type="text" class="form-control" name="inputNome" id="inputNome" placeholder="Nome" aria-required="true">
                </div>
                <div class="form-group col-md-6">
                    <label for="inputCpf">Cpf</label>
                    <input type="text" class="form-control" name="inputCpf" id="inputCpf" placeholder="000.000.000-00" aria-required="true">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputDataNasc">Data de Nascimento</label>
                    <input type="date" class="form-control" name="inputDataNasc" id="inputDataNasc" placeholder="dd-mm-aaaa" aria-required="true">
                </div>
                <div class="form-group col-md-6">
                    <label for="inputEmail">Email</label>
                    <input type="email" class="form-control" name="inputEmail" id="inputEmail" placeholder="Email" aria-required="true">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputTel">Telefone</label>
                    <input type="text" class="form-control" name="inputTel" id="inputTel" placeholder="(99) 9 9999-9999" aria-required="true">
                </div>
                <div class="form-group col-md-6">
                    <label for="inputAddress">Endereço</label>
                    <input type="text" class="form-control" name="inputAddress" id="inputAddress" placeholder="Rua, nº 0" aria-required="true">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="Cidade">Cidade</label>
                    <select name="Cidade" id="Cidade" class="form-control">

                    </select>
                </div>
                <div class="form-group col-md-2">
                    <label for="Estado">Estado</label>
                    <select name="Estado" id="Estado" class="form-control">

                    </select>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-2">
                    <button type="submit" class="btn btn-dark">Cadastrar</button>
                </div>
            </div>
        </form>
        <div class="col-md-2">
            <a href="http://127.0.0.1:8000/showAllUsers">
                <button class="btn btn-dark">
                    Listar Usuários
                </button>
            </a>
        </div>
    </div>
</div>
</body>
</html>


