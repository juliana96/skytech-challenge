<?php

namespace App\Http\Controllers;

use App\Models\ModelUser;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $usuario = new ModelUser();
        $usuario->usr_nome = $request->inputNome;
        $usuario->usr_email = $request->inputEmail;
        $usuario->usr_cpf = $request->inputCpf;
        $usuario->usr_data_nascimento = date('Y-m-d', strtotime($request->inputDataNasc));
        $usuario->usr_telefone = $request->inputTel;
        $usuario->usr_endereco = $request->inputAddress;
        $usuario->usr_cidade = $request->Cidade;
        $usuario->usr_estado = $request->Estado;
        $usuario->save();

        return redirect('http://127.0.0.1:8000/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    public function showAllUsers()
    {
        $usuarios = ModelUser::all();
        return view('showAllUsers', [
            'usuarios'=>$usuarios
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
